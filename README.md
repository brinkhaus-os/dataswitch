# dataswitch

# Overview
The EUProGigant Data Switch is an IoT-Gateway that brings machine data in the world of GaiaX.

It is aligned to the principles of
- Data sovereignty (complies with rights on data)
- Efficiency and speed (coded in C++, multi-threaded architecture)
- Near real-time-capabilities (supports gRPC live streaming of data)
- Ease of use (supports MQTT data distribution)

In the standard configuration, the Data Switch provides data delivered by so called Gateway Services into the GaiaX-network or in regular clouds.

# Introduction
You will find the introduction about the dataswitch in the wiki's sidebar under [Section 1](https://gitlab.com/brinkhaus-os/dataswitch/-/wikis/1.-Overview/1.1-Introduction).  

# Technical Details
You will find the technical details in the wiki's sidebar under [Section 2](https://gitlab.com/brinkhaus-os/dataswitch/-/wikis/2.-Technical-Details/2.1-System-Architecture).  
Details about the System architecture, a Runtime Tests system and the REST interface available are discussed in this section.

# Installation and Usage
How to install and use the dataswitch is available in this wiki's sidebar under [Section 3.1](https://gitlab.com/brinkhaus-os/dataswitch/-/wikis/3.-Installation/3.1-Installation-Steps), [Section 3.2](https://gitlab.com/brinkhaus-os/dataswitch/-/wikis/3.-Installation/3.2-Installation-as-Docker-Containers) and [Section 4](https://gitlab.com/brinkhaus-os/dataswitch/-/wikis/4.-Usage/4.1-Configuration).  

Section 3, which describes Installation, gives an account of the installation as debian packages as well as in the form of Docker containers.
Section 4, which describes Usage, gives an account of the configuration that can be done for running the dataswitch application, as well as details about retrieving the data via gRPC clients and MQTT. The UI data description subsection gives an account of how the UI can be modified and the UI components that can be set up on the UI.

# Support
Please reach out to brinkhaus@brinkhaus-gmbh.de for any support related queries.

